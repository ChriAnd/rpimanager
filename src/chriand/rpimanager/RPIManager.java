/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chriand.rpimanager;

import chriand.network.NetworkData;
import chriand.network.NetworkManager;
import chriand.network.PacketTypeCore;
import chriand.network.lan.IServerLANListener;
import chriand.network.lan.LocalAreaNetwork;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author Christoffer Andersson
 */
public class RPIManager extends Application implements IServerLANListener
{
    private VBox root;
    private boolean exit;
    private RaspberrypiSettings raspberrypiSettings;
    
    @Override
    public void start(Stage primaryStage)
    {
        LocalAreaNetwork.bindClient(this, "RPIHOST", 4444, 1000);
        this.root = new VBox();
        this.root.setSpacing(5);
        this.root.setPadding(new Insets(5));
        this.root.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
        
        Scene scene = new Scene(root, 500, 800);
        primaryStage.setTitle("Raspberry Pi Manager");
        primaryStage.setScene(scene);
        primaryStage.setOnCloseRequest(e -> 
        {
            LocalAreaNetwork.getInstance().shutdown();
        });
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public boolean onServerFound(String address, String userdata)
    {
        Platform.runLater(()->
        {
            this.root.getChildren().add(new RenderRaspberrypi(userdata, address, (ActionEvent event) ->
            {
                raspberrypiSettings = new RaspberrypiSettings(address, userdata);
            }));
        });
        return true;
    }

    @Override
    public void onServerNotFound()
    {
        if(!exit)
        {
            LocalAreaNetwork.bindClient(this, "RPIHOST", 4444, 0);
            Platform.runLater(()->
            {
                this.root.getChildren().clear();
            });
        }
    }
}