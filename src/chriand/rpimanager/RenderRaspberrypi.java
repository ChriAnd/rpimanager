/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chriand.rpimanager;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 *
 * @author Christoffer Andersson
 */
public class RenderRaspberrypi extends HBox
{
    public RenderRaspberrypi(String hostname, String address, EventHandler<ActionEvent> eventHandler)
    {
        VBox textBox = new VBox();
        HBox buttonBox = new HBox();
        
        this.widthProperty().addListener((Observable observable) ->
        {
            textBox.setPrefWidth(this.getWidth() / 2);
            buttonBox.setPrefWidth(this.getWidth() / 2);
        });
                
        Label hostnameLabel = new Label(hostname);
        Label addressLabel = new Label(address);
        
        hostnameLabel.setStyle("-fx-font-size: 25;-fx-font-family: Segoe UI Light;");
        addressLabel.setStyle("-fx-font-size: 20;-fx-font-family: Segoe UI Light;");
        
        Button button = new Button("Connect");
        button.setAlignment(Pos.CENTER_RIGHT);
        buttonBox.setAlignment(Pos.CENTER_RIGHT);
        button.setStyle("-fx-padding: 5 10 5 10;"
             + "-fx-border-width: 0;"
             + "-fx-background-radius: 0;"
             + "-fx-font-family: Segoe UI Light;"
             + "-fx-font-size: 15pt;"
             + "-fx-text-fill: #404040;");
        button.setBackground(new Background(new BackgroundFill(Color.gray(1F), CornerRadii.EMPTY, Insets.EMPTY)));
        button.setOnAction(eventHandler);
        
        textBox.getChildren().addAll(hostnameLabel, addressLabel);
        buttonBox.getChildren().addAll(button);
        this.getChildren().addAll(textBox, buttonBox);
        this.setPadding(new Insets(10));
        this.setBackground(new Background(new BackgroundFill(Color.gray(0.9F), CornerRadii.EMPTY, Insets.EMPTY)));
    }
}