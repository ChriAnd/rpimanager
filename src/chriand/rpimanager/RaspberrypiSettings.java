/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chriand.rpimanager;

import chriand.network.NetworkData;
import chriand.network.NetworkManager;
import chriand.network.PacketTypeCore;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Christoffer Andersson
 */
public class RaspberrypiSettings extends Stage
{   
    private Socket socket;
    
    public RaspberrypiSettings(String address, String name)
    {
        try
        {
            this.socket = new Socket();
            this.socket.connect(new InetSocketAddress(InetAddress.getByName(address), 4444));
            System.out.println("Kebab");
        }
        catch (IOException ex)
        {
            System.out.println("Snopp");
            return;
        }
        System.out.println("PAlle");
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
        gridPane.setHgap(5);
        gridPane.setVgap(5);
        
        gridPane.addRow(0, create5V());
        gridPane.addRow(0, create5V());
        gridPane.addRow(0, createGround());
        gridPane.addRow(0, createGPIO(15));
        gridPane.addRow(0, createGPIO(16));
        gridPane.addRow(0, createGPIO(1));
        gridPane.addRow(0, createGround());
        gridPane.addRow(0, createGPIO(4));
        gridPane.addRow(0, createGPIO(5));
        gridPane.addRow(0, createGround());
        gridPane.addRow(0, createGPIO(6));
        gridPane.addRow(0, createGPIO(10));
        gridPane.addRow(0, createGPIO(11));
        gridPane.addRow(0, createI2C());
        gridPane.addRow(0, createGround());
        gridPane.addRow(0, createGPIO(26));
        gridPane.addRow(0, createGround());
        gridPane.addRow(0, createGPIO(27));
        gridPane.addRow(0, createGPIO(28));
        gridPane.addRow(0, createGPIO(29));
        
        gridPane.addRow(1, create3V());
        gridPane.addRow(1, createGPIO(8));
        gridPane.addRow(1, createGPIO(9));
        gridPane.addRow(1, createGPIO(7));
        gridPane.addRow(1, createGround());
        gridPane.addRow(1, createGPIO(0));
        gridPane.addRow(1, createGPIO(2));
        gridPane.addRow(1, createGPIO(3));
        gridPane.addRow(1, create3V());
        gridPane.addRow(1, createGPIO(12));
        gridPane.addRow(1, createGPIO(13));
        gridPane.addRow(1, createGPIO(14));
        gridPane.addRow(1, createGround());
        gridPane.addRow(1, createI2C());
        gridPane.addRow(1, createGPIO(21));
        gridPane.addRow(1, createGPIO(22));
        gridPane.addRow(1, createGPIO(23));
        gridPane.addRow(1, createGPIO(24));
        gridPane.addRow(1, createGPIO(25));
        gridPane.addRow(1, createGround());
        
        Scene scene = new Scene(gridPane, 710, 75);
        this.setOnCloseRequest(e -> 
        {
            try
            {
                try
                {
                    NetworkManager.sendTCP(socket, new NetworkData(PacketTypeCore.CLOSE));
                }
                catch (IOException ex)
                {
                    Logger.getLogger(RaspberrypiSettings.class.getName()).log(Level.SEVERE, null, ex);
                }
                this.socket.close();
            }
            catch (IOException ex)
            {
                Logger.getLogger(RaspberrypiSettings.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        this.setTitle(name);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setScene(scene);
        this.setResizable(false);
        this.showAndWait();
    }
    
    private ToggleButton createGPIO(int button)
    {
        ToggleButton toggleButton = new ToggleButton(button + "");
        toggleButton.setUserData(button);
        toggleButton.setStyle("-fx-base: lightgreen;");
        toggleButton.setDisable(false);
        toggleButton.setPrefSize(30, 30);
        ToggleGroup group = new ToggleGroup();
        group.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle toggle, Toggle new_toggle) ->
        {
            if(!socket.isClosed())
            {
                if(new_toggle != null)
                {
                    try
                    {
                        NetworkManager.sendTCP(socket, new NetworkData(4, (new_toggle.getUserData() + "").getBytes(), "TRUE".getBytes()));
                    }
                    catch (IOException ex)
                    {
                        Logger.getLogger(RaspberrypiSettings.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.out.println("GPIO " + new_toggle.getUserData() + " = TRUE");
                }
                if(toggle != null)
                {
                    try
                    {
                        NetworkManager.sendTCP(socket, new NetworkData(4, (toggle.getUserData() + "").getBytes(), "FALSE".getBytes()));
                    }
                    catch (IOException ex)
                    {
                        Logger.getLogger(RaspberrypiSettings.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.out.println("GPIO " + toggle.getUserData() + " = FALSE");
                }
            }
        });
        toggleButton.setToggleGroup(group);
        return toggleButton;
    }
    
    private ToggleButton createGround()
    {
        ToggleButton toggleButton = new ToggleButton();
        toggleButton.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        toggleButton.setDisable(true);
        toggleButton.setPrefSize(30, 30);
        return toggleButton;
    }
    
    private ToggleButton create3V()
    {
        ToggleButton toggleButton = new ToggleButton();
        toggleButton.setBackground(new Background(new BackgroundFill(Color.ORANGE, CornerRadii.EMPTY, Insets.EMPTY)));
        toggleButton.setDisable(true);
        toggleButton.setPrefSize(30, 30);
        return toggleButton;
    }
    
    private ToggleButton create5V()
    {
        ToggleButton toggleButton = new ToggleButton();
        toggleButton.setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));
        toggleButton.setDisable(true);
        toggleButton.setPrefSize(30, 30);
        return toggleButton;
    }
    
    private ToggleButton createI2C()
    {
        ToggleButton toggleButton = new ToggleButton();
        toggleButton.setBackground(new Background(new BackgroundFill(Color.YELLOW, CornerRadii.EMPTY, Insets.EMPTY)));
        toggleButton.setDisable(true);
        toggleButton.setPrefSize(30, 30);
        return toggleButton;
    }
}
